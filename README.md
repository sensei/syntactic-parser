Syntactic parser for the SENSEI project
=======================================

2016-12-14 Benoit Favre <benoit.favre@lif.univ-mrs.fr>

This project includes a parser for the SENSEI project with models for English, French and Italian.

Building
--------

This will download and compile all the dependences, and install them to a prefix subdirectory
Then, python bindings for macaon and libraries are stored in the macaon subdirectory

```
./build-macaon.sh
./build-python.sh
```

Running
-------

The module can be used from the command line. It segments the text, tokenizes it and parses it with one of the available models.
```
cat text | python parser.py <lang=(en,fr,it)>
```

Format
------

The parser outputs a set of feature for each input word:
```
  end: the index of the last character of the token
  start: the index of the first character of the token
  id: the identifier of the token 
  num: the number of the word (referenced by the gov relation)
  text: the text of the token
  lemma: the lemma of the token
  pos: the part-of-speech of the token
  gov: the word num of the dependency governor
  dep_label: the dependency label
```

Example
-------

```
echo I would like to end wars. | python parser.py en
[
    [
        {
            "dep_label": "SBJ",
            "end": 1,
            "gov": 2,
            "id": 0,
            "lemma": "I",
            "num": 1,
            "pos": "PRP",
            "start": 0,
            "text": "I"
        },
        {
            "dep_label": "ROOT",
            "end": 7,
            "gov": 0,
            "id": 1,
            "lemma": "will",
            "num": 2,
            "pos": "MD",
            "start": 2,
            "text": "would"
        },
        {
            "dep_label": "VC",
            "end": 12,
            "gov": 2,
            "id": 2,
            "lemma": "like",
            "num": 3,
            "pos": "VB",
            "start": 8,
            "text": "like"
        },
        {
            "dep_label": "OPRD",
            "end": 15,
            "gov": 3,
            "id": 3,
            "lemma": "to",
            "num": 4,
            "pos": "TO",
            "start": 13,
            "text": "to"
        },
        {
            "dep_label": "IM",
            "end": 19,
            "gov": 4,
            "id": 4,
            "lemma": "end",
            "num": 5,
            "pos": "VB",
            "start": 16,
            "text": "end"
        },
        {
            "dep_label": "OBJ",
            "end": 24,
            "gov": 5,
            "id": 5,
            "lemma": "war",
            "num": 6,
            "pos": "NNS",
            "start": 20,
            "text": "wars"
        }
    ]
]
```

Python usage
------------

```
from __future__ import print_function
from parser import Parser
parser = Parser(language='en')
result = parser.process_document('This is a sample text. What is it about?')
for sentence in result:
    for token in result:
        print(token['num'], token['text'], token['gov'], token['dep_label'])
    print()
```

Training models
---------------

The training data is not delivered as part of this project and therefore retraining models is not fully supported.
For completeness, the build-models.sh script is provided.
