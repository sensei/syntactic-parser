from ctypes import *
import os

# bindings for macaon parser
# echo this is another test | MACAON_DIR=. MACAON_LIB=../build/libmacaon.so python2 macaon.py en

_backend = None
_data_dir = None
_library = None
_language = None
_old_model = False

def setup(language = 'en', data_dir = None, library = None, old_model=False):
    global _backend, _data_dir, _language

    _library = library or os.environ['MACAON_LIB']
    _data_dir = data_dir or os.environ['MACAON_DIR']
    _language = language
    _old_model = old_model

    # make sure MACAON_DIR is set before loading library
    os.environ['MACAON_DIR'] = _data_dir
    _backend = cdll.LoadLibrary(_library)

    if not os.path.exists(os.path.join(_data_dir, _language)):
        raise Exception('language \"%s\" not found in data_dir \"%s\"' % (_language, _data_dir))

    # macaon::Parser* Parser_new(char * cfg, int verbose_flag, char *model_file_name, char *alphabet_file_name, char *dep_count_file_name, int order);
    _backend.Parser_new.argtypes = [c_char_p, c_int, c_char_p, c_char_p, c_char_p, c_int]
    _backend.Parser_new.restype = c_void_p

    # void Parser_free(macaon::Parser* parser);
    _backend.Parser_free.argtypes = [c_void_p]
    _backend.Parser_free.restype = None

    # bool Parser_ProcessSentence(macaon::Parser* parser, int num_words, char** words, char** tags, char** lemmas, int* governors, const char** labels);
    _backend.Parser_ProcessSentence.argtypes = [c_void_p, c_int, POINTER(c_char_p), POINTER(c_char_p), POINTER(c_char_p), POINTER(c_int), POINTER(c_void_p)] # labels are c_char_p that need to be freed
    _backend.Parser_ProcessSentence.restype = c_bool

    # macaon::Tagger* Tagger_new(const char* modelName, const char* lexiconName);
    _backend.Tagger_new.argtypes = [c_char_p, c_char_p]
    _backend.Tagger_new.restype = c_void_p

    # void Tagger_free(macaon::Tagger* tagger);
    _backend.Tagger_free.argtypes = [c_void_p]
    _backend.Tagger_free.restype = None

    # bool Tagger_ProcessSentence(macaon::Tagger* tagger, int num_words, const char** words, const char** tags);
    _backend.Tagger_ProcessSentence.argtypes = [c_void_p, c_int, POINTER(c_char_p), POINTER(c_void_p)] # tags are c_char_p that need to be freed
    _backend.Tagger_ProcessSentence.restype = c_bool

    # macaon::Lemmatizer* Lemmatizer_new(char* cfg)
    _backend.Lemmatizer_new.argtypes = [c_char_p]
    _backend.Lemmatizer_new.restype = c_void_p

    # void Lemmatizer_free(macaon::Lemmatizer* lemmatizer);
    _backend.Lemmatizer_free.argtypes = [c_void_p]
    _backend.Lemmatizer_free.restype = None

    # char* Lemmatizer_Get(macaon::Lemmatizer* lemmatizer, char* word, char* tag);
    _backend.Lemmatizer_Get.argtypes = [c_void_p, c_char_p, c_char_p]
    _backend.Lemmatizer_Get.restype = c_void_p # c_char_p needs to be freed

    # free(void*);
    _backend.free.argtypes = [c_void_p]
    _backend.free.restype = None


class Tagger:
    def __init__(self, use_lexicon=True):
        global _backend, _data_dir, _language
        if _backend == None:
            raise Exception('macaon was not setup')

        self.backend = _backend

        lexicon = None
        if _old_model:
            model = '%s/%s/bin/crf_tagger_model_%s.bin' % (_data_dir, _language, _language)
            if use_lexicon:
                lexicon = '%s/%s/bin/crf_tagger_wordtag_lexicon_%s.bin' % (_data_dir, _language, _language)
        else:
            model = '%s/%s/bin/crf_tagger_model.bin' % (_data_dir, _language)
            if use_lexicon:
                lexicon = '%s/%s/bin/crf_tagger_wordtag_lexicon.bin' % (_data_dir, _language)
        self.tagger = self.backend.Tagger_new(model, lexicon)

    def process(self, words):
        num_words = len(words)
        c_words = (c_char_p * num_words)(*words)
        c_tags = (c_void_p * num_words)()
        result = self.backend.Tagger_ProcessSentence(self.tagger, num_words, c_words, c_tags)
        tags = []
        for c_tag in c_tags:
            tag = cast(c_tag, c_char_p).value
            self.backend.free(c_tag)
            tags.append(tag)
        if result:
            return zip(words, tags)
        raise Exception('failed to tag sentence')

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        self.backend.Tagger_free(self.tagger)


class Lemmatizer:
    def __init__(self):
        global _backend, _language
        if _backend == None:
            raise Exception('macaon was not setup')

        self.backend = _backend
        self.lemmatizer = self.backend.Lemmatizer_new(_language)

    def get(self, word, pos):
        c_lemma = self.backend.Lemmatizer_Get(self.lemmatizer, word, pos)
        lemma = cast(c_lemma, c_char_p).value
        self.backend.free(c_lemma)
        if lemma == '-':
            return word
        return lemma

    def process(self, tagged):
        return [(word, tag, self.get(word, tag)) for word, tag in tagged]

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        self.backend.Lemmatizer_free(self.lemmatizer)

class Parser:
    def __init__(self):
        global _backend, _data_dir, _language
        if _backend == None:
            raise Exception('macaon was not setup')

        self.backend = _backend

        order = 1
        if _old_model:
            model_stem = '%s/%s/bin/maca_graph_parser_model%d_%s' % (_data_dir, _language, 1, _language)
        else:
            model_stem = '%s/%s/bin/maca_graph_parser_model%d' % (_data_dir, _language, 1)
        verbose = 0
        weights = model_stem + '.bin'
        alpha = model_stem + '.alpha'
        dep_count = model_stem + '.dep_count'

        self.parser = self.backend.Parser_new(_language, verbose, weights, alpha, dep_count, order)

    def process(self, lemmatized):
        words, tags, lemmas = zip(*lemmatized)
        num_words = len(lemmatized)
        c_words = (c_char_p * num_words)(*words)
        c_tags = (c_char_p * num_words)(*tags)
        c_lemmas = (c_char_p * num_words)(*lemmas)
        c_governors = (c_int * num_words)()
        c_labels = (c_void_p * num_words)()

        result = self.backend.Parser_ProcessSentence(self.parser, num_words, c_words, c_tags, c_lemmas, c_governors, c_labels)

        labels = []
        for c_label in c_labels:
            label = cast(c_label, c_char_p).value
            self.backend.free(c_label)
            labels.append(label)

        if result:
            return zip([x + 1 for x in range(num_words)], words, tags, lemmas, c_governors, labels)
        raise Exception('failed to parse sentence')

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        self.backend.Parser_free(self.parser)


class Chain:
    def __init__(self):
        self.tagger = Tagger()
        self.lemmatizer = Lemmatizer()
        self.parser = Parser()

    def process(self, sentence, getter=lambda x: x):
        words = [getter(x) for x in sentence]
        tagged = self.tagger.process(words)
        lemmatized = self.lemmatizer.process(tagged)
        parsed = self.parser.process(lemmatized)
        return parsed
    
    def __enter__(self):
        self.tagger.__enter__()
        self.lemmatizer.__enter__()
        self.parser.__enter__()
        return self

    def __exit__(self, type, value, traceback):
        self.parser.__exit__(type, value, traceback)
        self.lemmatizer.__exit__(type, value, traceback)
        self.tagger.__exit__(type, value, traceback)



if __name__ == '__main__':
    import sys
    if len(sys.argv) != 2 or 'MACAON_DIR' not in os.environ or os.environ['MACAON_DIR'] == '' or 'MACAON_LIB' not in os.environ or os.environ['MACAON_LIB'] == '':
        sys.stderr.write('usage: echo <words> | %s <language>\n' % sys.argv[0])
        sys.stderr.write('MACAON_DIR shall point to the macaon model directory and MACAON_LIB to the macaon library (.so or .dll)\n')
        sys.exit(1)

    setup(language=sys.argv[1])

    #with Tagger() as tagger, Lemmatizer() as lemmatizer, Parser() as parser:
    #    for line in sys.stdin:
    #        words = line.strip().split()
    #        tagged = tagger.process(words)
    #        lemmatized = lemmatizer.process(tagged)
    #        parsed = parser.process(lemmatized)
    #        for i, word, tag, lemma, governor, label in parsed:
    #            print i, word, tag, lemma, governor, label
    #        print

    with Chain() as chain:
        for line in sys.stdin:
            parsed = chain.process(line.strip().split())
            for i, word, tag, lemma, governor, label in parsed:
                print i, word, tag, lemma, governor, label
            print

