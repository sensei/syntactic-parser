# -*- coding: utf8 -*-
import sys, collections, re

class Morpho:
    def __init__(self, filename='fplm.fr.txt', try_prefixes=False):
        self.dictionary = collections.defaultdict(list)
        self.try_prefixes = try_prefixes
        with open(filename) as fp:
            for line in fp:
                word, tag, lemma, morpho = line.decode('utf8').strip().split('\t')[:4]
                lemma = re.sub(r'^se ', '', lemma)
                lemma = re.sub(r"^s'", '', lemma)
                lemma = re.sub(r"^\(s'\)", '', lemma)
                self.dictionary[(word, tag)].append((lemma, morpho))

    def get(self, original_word, tag):
        word = original_word.lower()
        if (word, tag) in self.dictionary:
            fallback = 'NULL'
            for lemma, morpho in self.dictionary[(word, tag)]:
                if lemma != '-' and not (lemma.startswith('se ') or lemma.startswith("s'")):
                    return lemma, morpho
                else:
                    fallback = morpho
            return original_word, fallback
        if (re.search(r'^[rd]e[^aeiouyé]', original_word) or re.search(r'^[rd]é[aeiouyé]', original_word)) and (original_word[2:], tag) in self.dictionary:
            lemma, morpho = self.get(original_word[2:], tag)
            return original_word[:2] + lemma, morpho
        return original_word, 'NULL'

if __name__ == '__main__':
    morpho = Morpho()
    for line in sys.stdin:
        word, tag = line.decode('utf8').strip().split()
        print morpho.get(word, tag)
