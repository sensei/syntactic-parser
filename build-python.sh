#!/bin/bash

set -e -u -o pipefail

# These files are now tracked by git, we only need to get the libraries
#rm -rf macaon
#mkdir macaon
#cp macaon-master/python/macaon.py macaon/__init__.py

cp macaon-master/build/libmacaon.so macaon/
chrpath -r '$ORIGIN' macaon/libmacaon.so

cp prefix/lib/libfst.so.2 macaon/
cp prefix/lib/libgfsm.so.0 macaon/

