# -*- coding: utf-8 -*-

# Tokenizer that preserves token offsets
# Benoit Favre <benoit.favre@lif.univ-mrs.fr> 2015-01-07
# Code based on penn tokenizer http://www.cis.upenn.edu/~treebank/tokenizer.sed

# Note that remaining apostrophes inside tokens are not correcly handled

import re

mapping = {
        '(': '-LRB-',
        ')': '-RRB-',
        '[': '-LSB-',
        ']': '-RSB-',
        '{': '-LCB-',
        '}': '-RCB-',
        }

def Token(text, start, end):
    return {'text': text, 'start': start, 'end': end}

def finish(tokens, token, tokenStart, trim=0):
    if trim > 0:
        token = token[:-trim]
    if len(token) > 0:
        skip = False
        if '-' in token:
            parts = token.split('-')
            if parts[-1] in ['je', 'tu', 'il', 'elle', 'nous', 'vous', 'ils', 'elles', 'on']:
                word = '-'.join(parts[:-1])
                pronoun = '-' + parts[-1]
                tokens.append(Token(word, tokenStart, tokenStart + len(word)))
                tokens.append(Token(pronoun, tokenStart + len(word), tokenStart + len(word) + len(pronoun)))
            else:
                tokens.append(Token(token, tokenStart, tokenStart + len(token)))
        elif len(tokens) > 1 and tokens[-1]['text'].lower() == "aujourd'" and token.lower() == 'hui':
            tokens[-1]['text'] += token
            tokens[-1]['end'] += len(token)
        else:
            tokens.append(Token(token, tokenStart, tokenStart + len(token)))
        token = ''
    return token

def tokenize(text, skipTags=True, skipChars="+*^\n\r", keepChars='()', closingTag={'<': '>', '[': ']', '{': '}'}):
    tokens = []
    token = ''
    tokenStart = 0
    inTag = False
    tagType = None
    for i, c in enumerate(text):
        before = text[:i]
        after = text[i + 1:]

        if skipTags:
            if c in closingTag and not inTag:
                token = finish(tokens, token, tokenStart)
                inTag = True
                tagType = c
            elif inTag and c == closingTag[tagType]:
                inTag = False
                tokenStart = i + 1
                continue
        if inTag:
            continue

        if i == 0 and c == '"':
            token = finish(tokens, token, tokenStart)
            tokens.append(Token('``', 0, 1))
        elif i > 0 and text[i - 1] in ' ([{<' and c == '"':
            token = finish(tokens, token, tokenStart)
            tokens.append(Token('``', i, i + 1))
        elif c in skipChars:
            token = finish(tokens, token, tokenStart)
        elif c in keepChars:
            if len(token) == 0:
                tokenStart = i
            token += c
        elif c == '.' and before.endswith('..'):
            token = finish(tokens, token, tokenStart, 2)
            tokens.append(Token('...', i - 2, i + 1))
        elif c in ',;:@#$%&\n':
            token = finish(tokens, token, tokenStart)
            tokens.append(Token(c, i, i + 1))
        elif c in '?!':
            token = finish(tokens, token, tokenStart)
            tokens.append(Token(c, i, i + 1))
        elif c in '][(){}<>':
            token = finish(tokens, token, tokenStart)
            tokens.append(Token(mapping[c] if c in mapping else c, i, i + 1))
        elif text[:i + 1].endswith('--'):
            token = finish(tokens, token, tokenStart, 1)
            tokens.append(Token('--', i - 1, i + 1))
        elif c == '"':
            token = finish(tokens, token, tokenStart)
            tokens.append(Token("''", i, i + 1))
        elif c == "'" and not before.endswith("'"): #and after.startswith(' '):
            token = finish(tokens, token + "'", tokenStart)
            #tokens.append(Token("'", i, i + 1))
        elif c == '.' and re.match(r'[\])}>"\']*\s*$', after): # period at end of line
            token = finish(tokens, token, tokenStart)
            tokens.append(Token('.', i, i + 1))
        elif c == '.' and re.match(r'\s+[^a-zàéèëêçîïù_,?!]', after): # period before an upper case letter or sign
            token = finish(tokens, token, tokenStart)
            tokens.append(Token('.', i, i + 1))
        elif c == '.' and re.search(r'[a-zàéèëêçîïù_]{4}$', before): # period after a long word
            token = finish(tokens, token, tokenStart)
            tokens.append(Token('.', i, i + 1))
        elif c == ' ':
            token = finish(tokens, token, tokenStart)
        else:
            if len(token) == 0:
                tokenStart = i
            token += c

    finish(tokens, token, tokenStart)
    return tokens

def get_token_ids(text):
    ids = [-1 for x in text]
    speakers = [-1 for x in text]
    tokens = tokenize(text)
    for i, token in enumerate(tokens):
        start, end = token['start'], token['end']
        ids[start: end] = [i for x in range(end - start)]
    current_speaker = 0
    turn_speakers = [-1]
    for i in range(len(text)):
        found = re.match(r'<Turn [^>]*?speaker="([^"]*)"', text[i:])
        if found:
            turn_speakers = found.group(1).strip().split()
            current_speaker = 0
        found = re.match(r'<Who nb="([^"]*)"', text[i:])
        if found:
            current_speaker = int(found.group(1).strip()) - 1
            ids[i] = -2 # sentence end
        if text[i:].startswith('</Turn>'):
            turn_speakers = [-1]
            current_speaker = 0
            ids[i] = -2 # sentence end
        if current_speaker >= 0 and current_speaker < len(turn_speakers):
            speakers[i] = turn_speakers[current_speaker]
        else:
            speakers[i] = -1
            import sys
            print >>sys.stderr, 'ERROR: invalid speaker %d with list %s at offset %d' % (current_speaker, str(turn_speakers), i)
        found = re.match(r'<Sync time="([^"]*)"/><Sync time="([^"]*)"/>', text[i:])
        if found:
            first = float(found.group(1))
            second = float(found.group(2))
            #import sys
            #print >>sys.stderr, 'pause: %f' % (second - first)
            if second - first > 1:
                ids[i] = -2
    return ids, speakers

if __name__ == '__main__':
    #for text in ["\"That's my test',\" he'd have said.", "Cannot do it", "Hi, how are you?", "The Gov. doesn't do it (or what?), I don't."]:
    for text in ["C'est un<tag> petit +[pi] test aujourd'hui* pour moi-m(ême). Je n'en sais rien."]:
        print text
        print '-' * len(text)
        for token in tokenize(text):
            print token['text'], text[token['start']: token['end']], token['start'], token['end']
        print

