#!/bin/bash

set -e -u -o pipefail

rm -rf liblbfgs-1.10.tar.gz liblbfgs-1.10
wget https://github.com/downloads/chokkan/liblbfgs/liblbfgs-1.10.tar.gz
tar xvf liblbfgs-1.10.tar.gz
cd liblbfgs-1.10
./configure --prefix=$PWD/../prefix
make
make install
cd ..

rm -rf crfsuite-0.12.tar.gz crfsuite-0.12
wget https://github.com/downloads/chokkan/crfsuite/crfsuite-0.12.tar.gz
tar xvf crfsuite-0.12.tar.gz
cd crfsuite-0.12
./configure --prefix=$PWD/../prefix
make CFLAGS=-I$PWD/../prefix/include LDFLAGS="-L$PWD/../prefix/lib -llbfgs"
make install
cd ..

rm -rf gfsm-0.0.15-1.tar.gz gfsm-0.0.15-1
wget http://kaskade.dwds.de/~moocow/mirror/projects/gfsm/gfsm-0.0.15-1.tar.gz
tar xvf gfsm-0.0.15-1.tar.gz
cd gfsm-0.0.15-1
./configure --prefix=$PWD/../prefix
make
make install
cd ..

rm -rf openfst-1.5.1.tar.gz openfst-1.5.1
wget http://www.openfst.org/twiki/pub/FST/FstDownload/openfst-1.5.1.tar.gz
tar xvf openfst-1.5.1.tar.gz
cd openfst-1.5.1
./configure --prefix=$PWD/../prefix
make
make install
cd ..

rm -rf macaon-master
git clone https://gitlab.lif.univ-mrs.fr/benoit.favre/macaon.git macaon-master
cd macaon-master
git checkout 336c577b488e856d0cd61545df8a2478b00915af
mkdir build
cd build
cmake .. -DCMAKE_INSTALL_PREFIX=$PWD/../../prefix -DFST_PATH:PATH=$PWD/../../prefix -DGFSM_PATH:PATH=$PWD/../../prefix
make
make install

