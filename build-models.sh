#!/bin/bash

set -e -u -o pipefail

prefix=`readlink -f .`/prefix
export CFLAGS=-I$prefix/include
export PKG_CONFIG_PATH=$prefix/lib/pkgconfig
export PATH=$prefix/bin:$PATH
export LD_LIBRARY_PATH=$prefix/lib

target=$PWD/models
rm -rf $target
mkdir -p $target

# warning, this repository is not yet public
rm -rf maca_data
git clone git@gitlab.lif.univ-mrs.fr:talep/maca_data.git

cd maca_data
git checkout a055e7e5289c24c49df07812fc0f6acf66398fd0
cd tools
make
cd ..

for lang in en fr it
do
    cd $lang
    pwd
    make
    make install
    mkdir -p $target/$lang
    cp -r bin $target/$lang/
    cd ..
done
