from __future__ import print_function
import sys, os, re

dirname = os.path.dirname(__file__) or '.'
lib_dir = dirname + '/macaon'
macaon_dir = dirname + '/models'

import macaon
from tokenizer import tokenize


class Parser:
    def __init__(self, language):
        macaon.setup(language=language, data_dir=macaon_dir, library=lib_dir + '/libmacaon.so')
        self.language = language
        self.tagger = macaon.Tagger()
        self.lemmatizer = macaon.Lemmatizer()
        self.parser = macaon.Parser()

    def segment_sentences(self, tokens, speech_transcript=False):
        sentence = []
        for token in tokens:
            if token['text'] in '?!.\n':
                if len(sentence) > 0:
                    yield sentence
                sentence = []
            elif len(sentence) > 100:
                print('WARNING: cutting sentence because it is too long')
                yield sentence
                sentence = []
            elif token['text'] not in '.?!"\'()#,;:-' and token['text'] not in ['--', '...']:
                sentence.append(token)
        if len(sentence) > 0:
            yield sentence

    def process_document(self, text, speech_transcript = False):
        output = []
        word_id = 0
        if speech_transcript:
            tokens = tokenize(text, skipTags=False, skipChars='+*^\r', keepChars='')
        else:
            tokens = tokenize(text, skipTags=False, keepChars='')
        for sentence in self.segment_sentences(tokens, speech_transcript):
            if speech_transcript and len(sentence) > 2 and sentence[1] == ':':
                sentence = sentence[2:]
            words = [x['text'] for x in sentence]
            tagged = self.tagger.process(words)
            lemmatized = self.lemmatizer.process(tagged)
            parsed = self.parser.process(lemmatized)
            for word, info in zip(sentence, parsed):
                word['id'] = word_id
                word_id += 1
                word['num'], _, word['pos'], word['lemma'], word['gov'], word['dep_label'] = info
            output.append(sentence)
        return output


if __name__ == '__main__':
    if len(sys.argv) != 2:
        print('USAGE: cat text | %s <lang=(en,fr,it)>' % sys.argv[0], file=sys.stderr)
        sys.exit(1)
    parser = Parser(sys.argv[1])
    import json
    print(json.dumps(parser.process_document(sys.stdin.read()), sort_keys=True, indent=4, separators=(',', ': ')))

